package com.develop.model;

import java.util.Date;

public class Producto {

	private int idProducto;
	private String nombreProducto;
	private double costo;
	private Date fechaInsercion;
	
	public Producto() {

	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public Producto(int idProducto, String nombreProducto, double costo, Date fechaInsercion) {
		super();
		this.idProducto = idProducto;
		this.nombreProducto = nombreProducto;
		this.costo = costo;
		this.fechaInsercion = fechaInsercion;
	}

	public Date getFechaInsercion() {
		return fechaInsercion;
	}

	public void setFechaInsercion(Date fechaInsercion) {
		this.fechaInsercion = fechaInsercion;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public double getCosto() {
		return costo;
	}

	public void setCosto(double costo) {
		this.costo = costo;
	}
	
}
