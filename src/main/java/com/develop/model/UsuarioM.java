package com.develop.model;

public class UsuarioM {
	private String nombre;
	private String password;
	private String usuario;
	
	public UsuarioM() {
		super();

	}

	public UsuarioM(String nombre, String password, String usuario) {
		super();
		this.nombre = nombre;
		this.password = password;
		this.usuario = usuario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	

}
