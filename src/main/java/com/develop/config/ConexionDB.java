package com.develop.config;
import java.sql.Connection;
import java.sql.DriverManager;

public class ConexionDB {
	
	public static final String URL = "jdbc:mysql://localhost:3306/base_ecommerce";
    public static final String USER = "root";
    public static final String CLAVE = "";
    Connection con;
    
    public ConexionDB(){
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = (Connection) DriverManager.getConnection(URL,USER,CLAVE);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
    
    public Connection getConnection() {
    	return con;
    }

}
