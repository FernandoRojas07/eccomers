package com.develop.DAO;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jdt.internal.compiler.codegen.IntegerCache;

import com.develop.Interfaces.ProductoI;
import com.develop.config.ConexionDB;
import com.develop.model.Producto;
import com.develop.model.UsuarioM;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class ProductosDAO implements ProductoI{

	ConexionDB conexion = new ConexionDB();
	Connection consql;
	PreparedStatement ps;
	ResultSet rs;
	UsuarioM usuario = new UsuarioM();
	
	@Override
	public ArrayList<Producto> listaProducto() {
		String sql = "Select * from producto";
		ArrayList<Producto> productos = new ArrayList<>();
		try {
			consql = conexion.getConnection();
			ps = consql.prepareStatement(sql);
			rs = ps.executeQuery(); 
			while(rs.next()) {
				Producto producto = new Producto();
				producto.setIdProducto(rs.getInt("idProducto"));
				producto.setNombreProducto(rs.getString("nombreProducto"));
				producto.setCosto(rs.getDouble("costoProducto"));
				producto.setFechaInsercion(rs.getDate("fechaInsercion"));
				productos.add(producto);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		
		return productos;
	}
	
	@Override
	public String agregaProducto(String nombreProducto, String costoProducto) {
		// TODO Auto-generated method stub
		String mensaje = "";
		String fechaInsercion = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		System.out.println(fechaInsercion);
		String sql = "INSERT INTO producto (nombreProducto, costoProducto, fechaInsercion) VALUES ('" + nombreProducto + "', '" + costoProducto
				+ "', '" + fechaInsercion + "');";
		try {
			consql = conexion.getConnection();
			ps = consql.prepareStatement(sql);
			ps.execute();
			System.out.println("S� se pudo");
			mensaje = "OK";
		} catch (Exception e) {
			// TODO: handle exception
		}

		return mensaje;
	}
	@Override
	public String eliminiaProducto(int idProducto) {
		// TODO Auto-generated method stub
		String mensaje ="";
		String sql = "DELETE FROM producto WHERE idProducto = ?";
		System.out.println(sql);
		try {
			consql = conexion.getConnection();
			ps = consql.prepareStatement(sql);
			ps.setString(1, String.valueOf(idProducto));
			ps.executeUpdate();
			mensaje="OK";
		} catch (Exception e) {
			mensaje = " ";
			// TODO: handle exception
		}		
		return mensaje;
	}
	
	@Override
	public String editaProducto(int idProducto, String nombreProducto, double costoProducto) {
		// TODO Auto-generated method stub
		String mensaje ="";
		String sql = "UPDATE producto SET nombreProducto = ?" + ", costoProducto = ?" + " WHERE idProducto = ?";
		try {
			consql = conexion.getConnection();
			ps = consql.prepareStatement(sql);
			ps.setString(1, nombreProducto);
			ps.setString(2, String.valueOf(costoProducto));
			ps.setString(3, String.valueOf(idProducto));
			ps.executeUpdate();
			mensaje="OK";
		} catch (Exception e) {
			mensaje = " ";
			// TODO: handle exception
		}		
		return mensaje;
	}

	@Override
	public Producto buscaProducto(int idProducto) {
		String sql = "Select * from producto where idProducto= '" + idProducto + "'";
		Producto producto = new Producto();
		try {
			consql = conexion.getConnection();
			ps = consql.prepareStatement(sql);
			rs = ps.executeQuery();
			producto.setIdProducto(rs.getInt("idProducto"));
			producto.setNombreProducto(rs.getString("nombreProducto"));
			producto.setCosto(rs.getDouble("costoProducto"));
			producto.setFechaInsercion(rs.getDate("fechaInsercion"));
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return producto;
	}
	
	public void importatxt(){
		File carpeta = new File("C:/Users/lfern/Desktop/MiCarpeta");
        File archivo = new File(carpeta.getPath() + "/miArchivo.txt");
        String sql = "Select * from producto";
		Producto producto = new Producto();
        try {
        	if(!carpeta.exists()) {
        		if(carpeta.mkdir()) {
        			System.out.println("Directorio creado.");
        		}else {
        			System.out.println("Algo anda mal al crear el directorio");
        		}
        	}
	        boolean bandera = archivo.createNewFile();
	        consql = conexion.getConnection();
			ps = consql.prepareStatement(sql);
			rs = ps.executeQuery();
			if(bandera){
		        FileWriter fileWriter = new FileWriter(archivo);
		        ProductosDAO dao = new ProductosDAO();
				List<Producto> list= dao.listaProducto();
				Iterator<Producto> iter = list.iterator();
				Producto prod = null;
				fileWriter.write("idProducto \tNombre producto \tFecha Inserci�n \t costo \n");
				while(iter.hasNext()){
					int n=0;
					prod=iter.next();
					fileWriter.write(prod.getIdProducto() + "\t");
					fileWriter.write(prod.getNombreProducto() + "\t");
					fileWriter.write(prod.getFechaInsercion().toString() + "\t");
					fileWriter.write(prod.getCosto() + "\t\n");
				}
				
	            fileWriter.close();
	            System.out.println("Se cre� el carchivo con el contenido de los productos.");
            }else{
                System.out.println("No se pudo crear el archivo ni la carpeta.");
            }
        }catch (Exception e){
            System.out.println("Exception al crear el archivo " + e);
        }
	}
	
	public void importaPdf() {
		Document doc = new Document();
		System.out.println("Despues de doc");
		String sql ="Select * from producto";
		File carpeta = new File("C:/Users/lfern/Desktop/MiCarpeta");
		if(!carpeta.exists()) {
			if (carpeta.mkdirs()) {
				System.out.println("Directorio creado");
			}else {
				System.out.println("Error al crear directorio");
			}
		}
		try {
			consql = conexion.getConnection();
			ps = consql.prepareStatement(sql);
			rs = ps.executeQuery();
			PdfWriter.getInstance(doc, new FileOutputStream(carpeta.getPath() + "/Productos.pdf"));
			doc.open();
			Paragraph titulo = new Paragraph();
			Paragraph parrafo = new Paragraph();
			titulo.setAlignment(Paragraph.ALIGN_CENTER);
			parrafo.setAlignment(Paragraph.ALIGN_JUSTIFIED);
			titulo.add("Centro Comercial \n\n\n");
			parrafo.add("Productos existentes\\\n");
			doc.add(titulo);
			doc.add(parrafo);
			PdfPTable tabla = new PdfPTable(6);
			PdfPCell celda1 = new PdfPCell(new Paragraph("Id_Producto"));
            PdfPCell celda2 = new PdfPCell(new Paragraph("Nombre"));
            PdfPCell celda3 = new PdfPCell(new Paragraph("PrecioFecha de inserci�n"));
            PdfPCell celda4 = new PdfPCell(new Paragraph("Fecha de inserci�n"));
            tabla.addCell(celda1);
            tabla.addCell(celda2);
            tabla.addCell(celda3);
            tabla.addCell(celda4);
            while(rs.next()){
                tabla.addCell(rs.getInt(1)+"");
                tabla.addCell(rs.getString(2));
                tabla.addCell(rs.getDouble(3)+"");
                tabla.addCell(rs.getDate(4) + "\n");
                }
            doc.add(tabla);
            doc.close();
		} catch (Exception e) {
			
		}
	}
	
}
