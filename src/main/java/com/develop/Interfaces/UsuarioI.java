package com.develop.Interfaces;

public interface UsuarioI {

	public String validaUsuario(String nombre, String password);
	
	public String agregaUsuario(String nombre, String usuario, String password);
	
	public String eliminaUsuario(String nombre, String usuario);
	
}
