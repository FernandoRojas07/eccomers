package com.develop.Interfaces;

import java.util.Date;
import com.develop.model.Producto;
import java.util.ArrayList;

public interface ProductoI {

	public ArrayList<Producto> listaProducto();
	
	public String agregaProducto(String nombreProducto, String costoProducto);
	
	public String eliminiaProducto(int idProducto);
	
	public String editaProducto(int idProducto, String nombreProducto, double costoProducto);
	
	public Producto buscaProducto(int idProducto);
}
