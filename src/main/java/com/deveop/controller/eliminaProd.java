package com.deveop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.ProductosDAO;

/**
 * Servlet implementation class eliminaProd
 */
@WebServlet("/eliminaProd")
public class eliminaProd extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public eliminaProd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		int idProducto;
		idProducto = Integer.parseInt(request.getParameter("idProducto"));
		System.out.println("Hola, est�s eliminando el producto con id = " +  idProducto);
		ProductosDAO producto = new ProductosDAO();
		String respuesta = producto.eliminiaProducto(idProducto);
		if (respuesta == "OK") {
			System.out.println("Eliminando...");
			RequestDispatcher rq;
			rq = request.getRequestDispatcher("/Vistas/productos.jsp");
			rq.forward(request, response);
		}else {
			System.out.println("No se puede eliminar.");
			RequestDispatcher rq;
			rq = request.getRequestDispatcher("/Vistas/productos.jsp");
			rq.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
