package com.deveop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.ProductosDAO;

/**
 * Servlet implementation class AgregaProd
 */
@WebServlet("/AgregaProd")
public class AgregaProd extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AgregaProd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		String nombreProducto;
		String precioProducto;
		nombreProducto = request.getParameter("nombreProducto");
		precioProducto = request.getParameter("CostoProducto");
		System.out.println("el nombre de tu producto es: " +  nombreProducto + ", Y su precio es: " + precioProducto);
		ProductosDAO prod = new ProductosDAO();
		String respuesta = prod.agregaProducto(nombreProducto, precioProducto);
		if (respuesta=="OK") {
			System.out.println("Agregando...");
			RequestDispatcher rq;
			rq = request.getRequestDispatcher("/Vistas/productos.jsp");
			rq.forward(request, response);
		}else {
			System.out.println("No valido");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
