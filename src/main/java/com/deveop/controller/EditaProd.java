package com.deveop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.ProductosDAO;

/**
 * Servlet implementation class EditaProd
 */
@WebServlet("/EditaProd")
public class EditaProd extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public EditaProd() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		System.out.println(request.getParameter("CostoProducto"));
		int idProducto;
		idProducto = Integer.parseInt(request.getParameter("idProducto"));
		String nombreProducto = request.getParameter("nombreProducto");
		double costoProd = Double.parseDouble(request.getParameter("CostoProducto"));
		System.out.println("Hola, est�s editando " +  nombreProducto + " con un costo de " + costoProd);
		ProductosDAO producto = new ProductosDAO();
		System.out.println();
		String respuesta = producto.editaProducto(idProducto, nombreProducto, costoProd);
		if (respuesta == "OK") {
			System.out.println("Editando...");
			RequestDispatcher rq;
			rq = request.getRequestDispatcher("/Vistas/productos.jsp");
			rq.forward(request, response);
		}else {
			System.out.println("No se puede editar.");
			RequestDispatcher rq;
			rq = request.getRequestDispatcher("/Vistas/productos.jsp");
			rq.forward(request, response);
		}
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
