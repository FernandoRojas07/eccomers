package com.deveop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import com.develop.DAO.UsuarioDAO;
import com.develop.model.UsuarioM;

/**
 * Servlet implementation class Login
 */
@WebServlet(
		urlPatterns = { "/Login" })
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Hola desde Servet: ").append(request.getContextPath());
		//System.out.println(request.getParameter("name"));
		String usuario;
		String password;
		
		usuario = request.getParameter("usuario");
		password = request.getParameter("password");
		System.out.println(usuario + " " + password);
		UsuarioDAO dao = new UsuarioDAO();
		//UsuarioM per = null;
		String respuesta = dao.validaUsuario(usuario, password);
		RequestDispatcher rq;
		if(respuesta == "OK") {
			System.out.println("Redireccionando");
			rq = request.getRequestDispatcher("/Vistas/main.jsp");
			rq.forward(request, response);
		}
		else {
			System.out.println("No v�lido");
			rq = request.getRequestDispatcher("/Vistas/login.jsp");
			rq.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
