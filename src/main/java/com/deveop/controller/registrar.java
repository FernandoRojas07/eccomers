package com.deveop.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.UsuarioDAO;
import com.develop.model.UsuarioM;

/**
 * Servlet implementation class registrar
 */
@WebServlet("/registrar")
public class registrar extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public registrar() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Hola desde servlet: ").append(request.getContextPath());
		String nombre;
		String password;
		String usuario;
		nombre = request.getParameter("nombre");
		password = request.getParameter("password");
		usuario = request.getParameter("usuario");
		System.out.println("Tu nombre es: " +  nombre + ", tu usuario es: " + usuario + " y tu password es: " + password);
		UsuarioDAO user = new UsuarioDAO();
		String respuesta = user.agregaUsuario(nombre, usuario, password);
		if (respuesta=="OK") {
			System.out.println("Redireccionando...");
			RequestDispatcher rq;
			rq = request.getRequestDispatcher("/Vistas/login.jsp");
			rq.forward(request, response);
		}else {
			System.out.println("No valido");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
