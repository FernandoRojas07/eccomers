<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@include file="header.jsp"%>
<%@page import="com.develop.DAO.ProductosDAO"%>
<%@page import="com.develop.model.Producto"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/Vistas/productosStyle.css">
</head>
<body>
<div class="container">
    <div class="container-fluid p-0">
		<h1 class="h3 mb-3">Productos</h1>
		<div class="row">
			<div class="col-xl-8">
				<div class="card">
					<div class="card-header pb-0">
						<div class="card-actions float-right">
							<div class="dropdown show">
								<a class="btn btn-primary" href="<%=request.getContextPath()%>/Vistas/agregaProd.jsp">Agregar</a>
								<a href="#" data-toggle="dropdown" data-display="static">
									<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-horizontal align-middle"><circle cx="12" cy="12" r="1"></circle><circle cx="19" cy="12" r="1"></circle><circle cx="5" cy="12" r="1"></circle></svg>
								</a>
								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="#">Action</a>
									<a class="dropdown-item" href="#">Another action</a>
									<a class="dropdown-item" href="#">Something else here</a>
								</div>
							</div>
						</div>
						<h5 class="card-title mb-0">Productos</h5>
					</div>
					<div class="card-body">
						<table class="table table-striped" style="width:100%">
							<thead>
								<tr>
									<th>#</th>
									<th>Producto</th>
									<th>Fecha de inserción</th>
									<th>Costo</th>
									<th>       </th>
									<th>       </th>
								</tr>
							</thead>
								<%
									ProductosDAO dao = new ProductosDAO();
									List<Producto> list= dao.listaProducto();
									Iterator<Producto>iter = list.iterator();
									Producto prod = null;
									while(iter.hasNext()){
										int n=0;
										prod=iter.next();
                  				%>
							<tbody>
								<tr>
									<td><img src="https://bootdey.com/img/Content/avatar/avatar1.png" width="32" height="32" class="rounded-circle my-n1" alt="Avatar"></td>
									<td><%=prod.getNombreProducto()%></p></td>
									<td><%=prod.getFechaInsercion()%></td>
									<td><%=prod.getCosto()%></td>
									<td><a class="btn btn-primary" href="<%=request.getContextPath()%>/Vistas/editaProducto.jsp?idProducto=<%=prod.getIdProducto()%>&nombreProducto=<%=prod.getNombreProducto()%>&CostoProducto=<%=prod.getCosto()%>">Editar</a></td>
									<td><form method="POST" action="<%=request.getContextPath()%>/eliminaProd?idProducto=<%=prod.getIdProducto()%>"><button type="submit" class="btn btn-danger">Eliminar</button></form></td>
								</tr>
								<%} %>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
</body>
</html>