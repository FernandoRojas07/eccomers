<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@include file="header.jsp"%>
<%@page import="com.develop.DAO.ProductosDAO"%>
<%@page import="com.develop.model.Producto"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
<link rel="stylesheet" href="<%=request.getContextPath()%>/Vistas/productosStyle.css">
<title>Insert title here</title>
</head>
<body>

<form method="POST" action="<%=request.getContextPath()%>/EditaProd">

	<div class="form-group"> <!-- Full Name -->
        <label for="full_name_id" class="control-label">Id de producto: </label>
        <input type="text" class="form-control" id="full_name_id" name="idProducto" readonly value="<%=request.getParameter("idProducto")%>">
    </div>
    
    <div class="form-group"> <!-- Full Name -->
        <label for="full_name_id" class="control-label">Nombre del producto: </label>
        <input type="text" class="form-control" id="full_name_id" name="nombreProducto" placeholder="<%=request.getParameter("nombreProducto")%>">
    </div>    

    <div class="form-group"> <!-- Street 1 -->
        <label for="street1_id" class="control-label">Precio: </label>
        <input type="text" class="form-control" id="street1_id" name="CostoProducto" placeholder="<%=request.getParameter("CostoProducto")%>">
    </div>
                             
<!--     <div class="form-group"> State Button -->
<!--         <label for="state_id" class="control-label">State</label> -->
<!--         <select class="form-control" id="state_id"> -->
<!--             <option value="AL">Alabama</option> -->
<!--             <option value="AK">Alaska</option> -->
<!--             <option value="AZ">Arizona</option> -->
<!--             <option value="AR">Arkansas</option> -->
<!--             <option value="CA">California</option> -->
<!--             <option value="CO">Colorado</option> -->
<!--             <option value="CT">Connecticut</option> -->
<!--             <option value="DE">Delaware</option> -->
<!--             <option value="DC">District Of Columbia</option> -->
<!--             <option value="FL">Florida</option> -->
<!--             <option value="GA">Georgia</option> -->
<!--             <option value="HI">Hawaii</option> -->
<!--             <option value="ID">Idaho</option> -->
<!--             <option value="IL">Illinois</option> -->
<!--             <option value="IN">Indiana</option> -->
<!--             <option value="IA">Iowa</option> -->
<!--             <option value="KS">Kansas</option> -->
<!--             <option value="KY">Kentucky</option> -->
<!--             <option value="LA">Louisiana</option> -->
<!--             <option value="ME">Maine</option> -->
<!--             <option value="MD">Maryland</option> -->
<!--             <option value="MA">Massachusetts</option> -->
<!--             <option value="MI">Michigan</option> -->
<!--             <option value="MN">Minnesota</option> -->
<!--             <option value="MS">Mississippi</option> -->
<!--             <option value="MO">Missouri</option> -->
<!--             <option value="MT">Montana</option> -->
<!--             <option value="NE">Nebraska</option> -->
<!--             <option value="NV">Nevada</option> -->
<!--             <option value="NH">New Hampshire</option> -->
<!--             <option value="NJ">New Jersey</option> -->
<!--             <option value="NM">New Mexico</option> -->
<!--             <option value="NY">New York</option> -->
<!--             <option value="NC">North Carolina</option> -->
<!--             <option value="ND">North Dakota</option> -->
<!--             <option value="OH">Ohio</option> -->
<!--             <option value="OK">Oklahoma</option> -->
<!--             <option value="OR">Oregon</option> -->
<!--             <option value="PA">Pennsylvania</option> -->
<!--             <option value="RI">Rhode Island</option> -->
<!--             <option value="SC">South Carolina</option> -->
<!--             <option value="SD">South Dakota</option> -->
<!--             <option value="TN">Tennessee</option> -->
<!--             <option value="TX">Texas</option> -->
<!--             <option value="UT">Utah</option> -->
<!--             <option value="VT">Vermont</option> -->
<!--             <option value="VA">Virginia</option> -->
<!--             <option value="WA">Washington</option> -->
<!--             <option value="WV">West Virginia</option> -->
<!--             <option value="WI">Wisconsin</option> -->
<!--             <option value="WY">Wyoming</option> -->
<!--         </select>                     -->
<!--     </div>         -->
    
    <div class="form-group"> <!-- Submit Button -->
        <button type="submit" class="btn btn-primary">Confirmar</button>
        <a type="submit" class="btn btn-danger" href="<%=request.getContextPath()%>/Vistas/productos.jsp">Cancelar</a>
    </div>     
    
</form>
<%
		String idProducto = request.getParameter("idProducto");
    	String nombreProducto = request.getParameter("nombreProducto");
    	String CostoProducto = request.getParameter("CostoProducto");
    	System.out.print(CostoProducto);
%>
</body>
</html>