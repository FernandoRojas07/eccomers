<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@include file="header.jsp"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Importar</title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/Vistas/productosStyle.css">
</head>
<body>
<form method="POST" action="<%=request.getContextPath()%>/ImportaProductos">
	<div>
		<td>
			<button type="submit" class="btn btn-primary" name="button" value="importatxt">Importar productos txt</button>
		</td>
		<td>
			<button type="submit" class="btn btn-primary" name="button" value="importapdf">Importar productos PDF</button>
		</td>
	</div>
</form>
</body>
</html>